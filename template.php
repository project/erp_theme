<?php

/**
 * @file
 *
 * Example template.php with overrides for common things on quote, invoice, cash_sale and purchase_order
 * 
 * This version does not show the "GST/Tax" line on the invoice/quote/etc
 */

/**
 * Let the theme system know what we can provide
 */
function erp_theme_theme($existing, $type, $theme, $path) {
  return array(
    'erp_quote_info' => array(
      'arguments' => array('node' => NULL),
    ),
    'erp_invoice_info' => array(
      'arguments' => array('node' => NULL),
    ),
    'erp_cash_sale_info' => array(
      'arguments' => array('node' => NULL),
    ),
    'erp_purchase_order_info' => array(
      'arguments' => array('node' => NULL),
    ),
  );
}

/**
 * Various functions to implement our themes
 * $node->printing is set by the print module
 */
function erp_theme_erp_invoice_info($node) {
  if ($node->printing) {

    // Mark the invoice as having been printed
    erp_invoice_printed($node);

    // Return the print information
    return erp_theme_erp_print_info($node);
  }
  else {
    // Return the screen information
    return erp_theme_erp_screen_info($node);
  }
}

function erp_theme_erp_quote_info($node) {
  if ($node->printing) {
    return erp_theme_erp_print_info($node);
  }
  else {
    return erp_theme_erp_screen_info($node);
  }
}

function erp_theme_erp_cash_sale_info($node) {
  if ($node->printing) {
    return erp_theme_erp_print_info($node);
  }
  else {
    return erp_theme_erp_screen_info($node);
  }
}

function erp_theme_erp_purchase_order_info($node) {
  if ($node->printing) {
    return erp_theme_erp_print_info($node);
  }
  else {
    return erp_theme_erp_screen_info($node);
  }
}

/**
 * On screen info includes everything
 */
function erp_theme_erp_screen_info($node) {
  $payments = array();
  $header = array();

  $type = str_replace('erp_', '', $node->type);

  $output = '<div class="'. $type .'-info">';

  $output .= '<div class="'. $type .'-id">';
  $output .= t('@type no: ', array('@type' => ucfirst(str_replace('_', ' ', $type)))) . $node->{$type .'_id'};
  if ($node->type == 'erp_invoice') {
    if ($node->po) {
      $output .= ' - '. t('purchase order: ') . $node->po;
    }
  }
  $output .= '</div>';

  switch ($node->type) {
    case 'erp_invoice':
      $output .= '<div class="printed">';
      $output .= t('Printed: ') . ($node->printed ? t('Yes') : t('No'));
      $output .= '</div>';

      // Invoice? paid?
      if (!_erp_payment_get_balance($node) && $node->invoice_status == 'C') {
        $output .= '<div class="paid">'. theme('image', drupal_get_path('module', 'erp') .'/images/erp_paid.png') .'</div>';
      }
      break;

    case 'erp_quote':
      // Quote closed?
      if ($node->quote_status == 'C') {
        $output .= '<div class="closed">'. theme('image', drupal_get_path('module', 'erp') .'/images/erp_closed.png') .'</div>';
      }
      break;
  }

  $output .= implode(module_invoke_all('erp_extra_info', $node));
  $output .= implode(module_invoke_all($node->type .'_info', $node));

  list($header, $rows, $total_price) = erp_theme_erp_item_view_list($node);

  if ($node->type == 'erp_invoice') {
    if (erp_invoice_is_open($node)) {
      if (module_exists('erp_payment')) {
        if (_erp_payment_get_balance($node) != $total_price) {
          $row = array_fill(0, count($header) - 2, array());
          $row[] = array('data' => t("Outstanding: "), 'class' => 'outstanding-text');
          $row[] = array('data' => "$ ". erp_currency(_erp_payment_get_balance($node)), 'class' => 'outstanding-amount');
          $rows[] = $row;
        }
      }
    }
  }

  $output .= '<p>'. theme('table', $header, $rows) .'</p>';

  if ($node->extra_notes) {
    $output .= '<p><strong>'. t('Extra notes') .'</strong></p>';
    $output .= '<p>'. $node->extra_notes .'</p>';
  }

  if ($node->type == 'erp_invoice') {
    $output .= erp_invoice_outstanding($node);
  }

  $output .= '</div>';

  return $output;
}

/**
 * When printing, there is different info shown, and the structure is quite precise
 * TODO: Make some sort of template system, or just change to use print.tpl.php
 * based system?
 */
function erp_theme_erp_print_info($node) {
  $payments = array();
  $header = array();

  $type = str_replace('erp_', '', $node->type);

  $customer = node_load($node->customer_nid);
  $store = node_load(erp_id_to_nid('store', $node->store_id));
  $filename = module_invoke('erp_store', 'erp_store_logo', $store);

  $output = '<div class="'. $type .'-info">';

  $output .= '<div class="logo"><img src="'. $filename .'" alt="'. $store->company .'" /></div>';
  $output .= '<div class="company-address">';
  if ($store->company) {
    $output .= $store->company .'<br />';
  }
  if ($store->address) {
    $output .= $store->address .'<br />';
  }
  if ($store->suburb || $store->state || $store->postcode) {
    $output .= $store->suburb .' '. $store->state .' '. $store->postcode .'<br />';
  }
  if ($store->phone) {
    $output .= 'Phone '. $store->phone .'<br />';
  }
  if ($store->fax) {
    $output .= 'Fax '. $store->fax;
  }
  $output .=  '</div>';

  switch ($node->type) {
  case 'erp_invoice':
    // Invoice? paid?
    if (!_erp_payment_get_balance($node) && $node->invoice_status == 'C') {
      $output .= '<div class="paid">'. theme('image', drupal_get_path('module', 'erp') .'/images/erp_paid.png') .'</div>';
    }

    // Invoice details
    $type_header = '<h1>Tax&nbsp;Invoice</h1>';
    $type_id = 'Invoice no: '. $node->invoice_id .'</div>';
    break;
  case 'erp_quote':
    // Quote closed?
    if ($node->quote_status == 'C') {
      $output .= '<div class="closed">'. theme('image', drupal_get_path('module', 'erp') .'/images/erp_closed.png') .'</div>';
    }

    $type_header = '<h1>Quote</h1>';
    $type_id = 'Quote no: '. $node->quote_id .'</div>';
    break;
  case 'erp_purchase_order':
    $type_header = '<h1>Purchase order</h1>';
    $type_id = 'Purchase order no: '. $node->purchase_order_id .'</div>';
    $supplier = node_load($node->supplier_nid);
    break;
  case 'erp_cash_sale':
    $type_header = '<h1>Cash sale</h1>';
    $type_id = 'Cash sale no: '. $node->cash_sale_id .'</div>';
    break;
  }

  $output .= '<div class="details">'.
    $type_header .'<br />'.
    'ABN: '. $store->tax_id .'<br />'.
    'Date: '. format_date($node->created, 'custom', 'd-m-Y') .'<br />'.
    $type_id;

  if ($node->type != 'erp_purchase_order' && $node->type != 'erp_goods_receive') {
    // Store Details
    $output .= '<div class="bill_to">';
    $output .= '<strong>Bill to</strong><br />';
    if ($customer->title) {
      $output .= check_plain($customer->title) .'<br />';
    }
    if ($customer->postal_address) {
      $output .= check_plain($customer->postal_address) .'<br />';
    }
    if ($customer->postal_suburb) {
      $output .= check_plain($customer->postal_suburb) .'<br />';
    }
    if ($customer->postal_state) {
      $output .= check_plain($customer->postal_state) .'&nbsp;&nbsp;&nbsp;';
    }
    if ($customer->postal_postcode) {
      $output .= check_plain($customer->postal_postcode);
    }
    $output .= '</div>';

    // Render terms etc.
    $header = array('PO No', 'Terms', 'Due Date', 'Rep');
    $rows = array();
    $rows[] = array($node->po, erp_accounting_get_terms($node), $node->due_date, $node->rep);
    $output .= '<div class="info_pane">'. theme('table', $header, $rows, array('class' => 'info_pane')) .'</div>';
  }
  else {
    // Customer Details
    $output .= '<div class="bill_to">';
    $output .= '<strong>Order for</strong><br />';
    if ($store->title) {
      $output .= check_plain($store->title) .'<br />';
    }
    if ($store->address) {
      $output .= check_plain($store->address) .'<br />';
    }
    if ($store->suburb) {
      $output .= check_plain($store->suburb) .'<br />';
    }
    if ($store->state) {
      $output .= check_plain($store->state) .'&nbsp;&nbsp;&nbsp;';
    }
    if ($store->postcode) {
      $output .= check_plain($store->postcode);
    }
    $output .= '</div>';

    // Render terms etc.
    $header = array('Supplier', 'Phone', 'Email');
    $rows = array();
    $rows[] = array($supplier->title, $supplier->phone, $supplier->email);
    $output .= '<div class="info_pane">'. theme('table', $header, $rows, array('class' => 'info_pane')) .'</div>';
  }

  $output .= '</div>';

  list($header, $rows, $total_price) = erp_theme_erp_item_view_list($node);

  if ($node->type == 'erp_invoice') {
    if (erp_invoice_is_open($node)) {
      if (module_exists('erp_payment')) {
        if (_erp_payment_get_balance($node) != $total_price) {
          $row = array_fill(0, count($header) - 2, array());
          $row[] = array('data' => t("Outstanding: "), 'class' => 'outstanding-text');
          $row[] = array('data' => "$ ". erp_currency(_erp_payment_get_balance($node)), 'class' => 'outstanding-amount');
          $rows[] = $row;
        }
      }
    }
  }

  if ($node->type == 'erp_cash_sale') {
    $row = array_fill(0, count($header) - 2, array());
    $row[] = array('data' => t("Payment: "), 'class' => 'payment-text');
    $row[] = array('data' => erp_payment_type_desc($node->payment_type) ." - $ ". erp_currency($node->payment_amount));
    $rows[] = $row;
  }
  
  $output .= '<p>'. theme('table', $header, $rows) .'</p>';

  if ($node->extra_notes) {
    $output .= '<p><strong>'. t('Extra notes') .'</strong></p>';
    $output .= '<p>'. $node->extra_notes .'</p>';
  }
  
  // Provide banking details on invoice
  if ($node->type == 'erp_invoice') {
    $output .= erp_invoice_outstanding($node);

    // Bank Details and contact details
    $output .= '<div class="bank-details">';
    if ($store->company) {
      $output .= 'Bank Account Details<br />'.
        'Account Name: '. $store->company .'<br />'.
        $store->bank_details .'<br />';
    }
    $output .= '</div>';
  }

  $output .= '<div class="contact-details">';
  if ($store->phone) {
    $output .= 'Phone: '. $store->phone .'<br />';
  }
  if ($store->fax) {
    $output .= 'Fax: '. $store->fax .'<br />';
  }
  if ($store->email) {
    $output .= 'Email: '. $store->email .'<br />';
  }
  if ($store->homepage) {
    $output .= 'Web: '. $store->homepage;
  }
  $output .= '</div>';

  $output .= '</div>';

  return $output;
}

/**
 * Implement or own version of the item list that doesn't show tax
 * 
 */
function erp_theme_erp_item_view_list($node, $show_tax = FALSE) {
  $header[] = t('Stock Code');
  $header[] = t('Qty');
  if ($node->type == 'erp_invoice') {
    $header[] = t('Date');
  }
  $header[] = t('Desc');
  if (module_exists('erp_stock') && !$node->printing) {
    $header[] = t('Avail');
  }
  if (variable_get('erp_invoice_serial', FALSE) && $node->type == 'erp_invoice') {
    $header[] = t('Serial no.');
  }
  $header[] = t('Price');
  $header[] = t('Line total');

  $rows = array();

  for ($i = 0 ; $i < count($node->entries['qty']) ; $i++) {
    $row = array();

    $item_node = node_load($node->entries['item'][$i]);
    if ($item_node) {
      $desc = check_plain($item_node->title);
      if (user_access('item edit')) {
        $code = l($item_node->code, 'node/'. $item_node->nid);
      }
      else {
        $code = $item_node->code;
      }
    }
    else {
      $desc = "Unknown item node for ". $node->entries['item'][$i];
    }
    if ($node->entries['extra'][$i]) {
      $desc .= '<br />'. $node->entries['extra'][$i];
    }

    $row[] = array('data' => $code, 'class' => 'item-code');
    $row[] = array('data' => $node->entries['qty'][$i], 'class' => 'item-qty');
    if ($node->type == 'erp_invoice') {
      $row[] = array('data' => date_format_date(date_make_date($node->entries['completed_date'][$i], NULL, DATE_UNIX), 'custom', 'Y-m-d'), 'class' => 'item-date-completed');
    }
    $row[] = array('data' => $desc, 'class' => 'item-class');

    if (module_exists('erp_stock') && !$node->printing) {
      $avail = erp_stock_avail($node->entries['item'][$i]);
      $row[] = array('data' => $avail, 'class' => 'item-avail');
    }
    if (variable_get('erp_invoice_serial', FALSE) && $node->type == 'erp_invoice') {
      $row[] = array('data' => $node->entries['serial'][$i]);
    }

    $row[] = array('data' => erp_currency($node->entries['price'][$i]), 'class' => 'item-amount');
    $row[] = array('data' => erp_currency($node->entries['price'][$i] * $node->entries['qty'][$i]), 'class' => 'item-subtotal');
    $rows[] = $row;

    // Build a total price
    $total_price += $node->entries['price'][$i] * $node->entries['qty'][$i];
  }

  // Dont show the tax amount
  //$row = array_fill(0, count($header) - 2, array());
  //$row[] = array('data' => 'GST total: ', 'class' => 'tax-total-text');
  //$row[] = array('data' => erp_currency(array_pop(module_invoke_all('erp_tax_amount', $total_price))));
  //$rows[] = $row;

  $row = array_fill(0, count($header) - 2, array());
  $row[] = array('data' => "Total: ", 'class' => 'total-text');
  $row[] = array('data' => erp_currency($total_price), 'class' => 'total-amount');

  $rows[] = $row;
  $output .= theme('table', $header, $rows, array('class' => 'item-list'));

  return array($header, $rows, $total_price);
}
